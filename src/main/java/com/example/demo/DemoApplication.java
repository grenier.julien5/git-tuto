package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here! Wouhou!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public String getToken(){
		return "1";
	}

	public String getKey(){
		return "ighae/saeX5ua=neif4b";
	}

	public String getName(){
		return "My name";
	}

	public Integer getNumber1(){
		return 1;
	}

	public Integer getNumber2(){
		return 2;
	}

	public Integer getNumber3(){
		return 3;
	}

	public Integer getNumber4(){
		return 4;
	}

	public String getString5a(){
		return "5a";
	}

	public String getString5b(){
		return "5b";
	}
}